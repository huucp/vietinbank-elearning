#ver 031117
CREATE TABLE `ke_hoach` (
 `ma_bai_giang` char(7) COLLATE utf8_unicode_ci NOT NULL,
 `ten_bai_giang` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
 `ma_thay_the` char(7) COLLATE utf8_unicode_ci NOT NULL,
 `loai_ke_hoach` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
 `muc_tieu` text COLLATE utf8_unicode_ci NOT NULL,
 `doi_tuong` text COLLATE utf8_unicode_ci NOT NULL,
 `noi_dung` text COLLATE utf8_unicode_ci NOT NULL,
 `thoi_gian_nhan_slide` char(10) COLLATE utf8_unicode_ci NOT NULL,
 `thoi_gian_bat_dau` char(10) COLLATE utf8_unicode_ci NOT NULL,
 `thoi_gian_hoan_thanh` char(10) COLLATE utf8_unicode_ci NOT NULL,
 `phong_ban_de_xuat` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
 `can_bo_nghiep_vu` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
 `can_bo_ngan_hang` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
 `can_bo_it` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
 `lanh_dao_phu_trach` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
 `huy` tinyint(1) NOT NULL DEFAULT '0',
 PRIMARY KEY (`ma_bai_giang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci