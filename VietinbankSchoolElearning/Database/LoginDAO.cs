﻿using System.Data;
using System.Reflection;
using log4net;
using MySql.Data.MySqlClient;

namespace VietinbankSchoolElearning.Database
{
    public class LoginDAO
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static bool Login(string username, string password)
        {
            using (var connection = DatabaseConnection.Instance.GetConnection())
            {
                try
                {
                    var command = new MySqlCommand
                            {
                                Connection = connection,
                                CommandText = "select * from user where email=@email and mat_khau=@password"
                            };
                    command.Parameters.AddWithValue("@email", username);
                    command.Parameters.AddWithValue("@password", password);
                    var reader = command.ExecuteReader();
                    var dt = new DataTable();
                    dt.Load(reader);
                    return dt.Rows.Count > 0;
                }
                catch (System.Exception e)
                {
                    Logger.Error(e);
                    return false;
                }
            }
        }
    }
}