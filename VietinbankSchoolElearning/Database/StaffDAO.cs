﻿using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Windows.Documents;
using log4net;
using MySql.Data.MySqlClient;
using VietinbankSchoolElearning.DataModel;

namespace VietinbankSchoolElearning.Database
{
    public class StaffDAO
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static bool AddUser(string email, string password, UserType userType)
        {
            using (var connection = DatabaseConnection.Instance.GetConnection())
            {
                try
                {
                    var cmd = new MySqlCommand
                            {
                                Connection = connection,
                                CommandText = "insert into user (email,mat_khau,loai) " +
                                              "select * from (select @email,@password,@userType) as tmp " +
                                              "where exists (select * from can_bo where email=@email)"
                            };
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@password", password);
                    cmd.Parameters.AddWithValue("@userType", (int)userType);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (System.Exception exception)
                {
                    Logger.Error(exception);
                    return false;
                }
            }
        }

        public static bool DeleteUser(string email)
        {
            using (var connection = DatabaseConnection.Instance.GetConnection())
            {
                try
                {
                    var cmd = new MySqlCommand
                    {
                        Connection = connection,
                        CommandText = "delete from user where email=@email"
                    };
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (System.Exception exception)
                {
                    Logger.Error(exception);
                    return false;
                }
            }
        }

        public static List<StaffData> GetStaffs()
        {
            var list = new List<StaffData>();
            using (var connection = DatabaseConnection.Instance.GetConnection())
            {
                try
                {
                    var cmd = new MySqlCommand
                    {
                        Connection = connection,
                        CommandText = "select macanbo,hoten,gioitinh,vitri,dienthoai,a.email,atm,b.loai from can_bo a inner join user b on a.email=b.email"
                    };
                    MySqlDataReader reader = cmd.ExecuteReader();
                    var dt = new DataTable();
                    dt.Load(reader);
                    foreach (DataRow row in dt.Rows)
                    {
                        var user = new StaffData
                        {
                            Id = row["macanbo"].ToString(),
                            Name = row["hoten"].ToString(),
                            Gender = row["gioitinh"].ToString(),
                            Title = row["vitri"].ToString(),
                            Phone = row["dienthoai"].ToString(),
                            Email = row["email"].ToString(),
                            AccountNumber = row["atm"].ToString(),
                        };

                        var i = int.Parse(row["loai"].ToString());
                        user.StaffType = (UserType) i;
                        list.Add(user);
                    }
                }
                catch (System.Exception exception)
                {
                    Logger.Error(exception);
                }
                return list;
            }
        }
    }
}