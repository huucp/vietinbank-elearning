﻿using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using log4net;
using MySql.Data.MySqlClient;
using VietinbankSchoolElearning.Utils;

namespace VietinbankSchoolElearning.Database
{
    public class DatabaseConnection
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public const int QueryFailCode = -1;
        public const int MySqlTransactionLimit = 1000;
        private static readonly DatabaseConnection instance = new DatabaseConnection();

        static DatabaseConnection() { }

        private DatabaseConnection()
        {
            OpenConnect();

        }

        public static DatabaseConnection Instance
        {
            get
            {
                return instance;
            }
        }


        private MySqlConnection _conn;
        private void OpenConnect()
        {
            try
            {

                string ip = Config.ServerIp;
#if DEBUG
                var sqlConnect = @"Server=" + ip + ";Database=elearning;User=elearning;Pwd=nhthvtb;CharSet=utf8;";
#else
                var sqlConnect = @"Server=" + ip + ";Database=elearning;User=elearning;Pwd=nhthvtb;charset=utf8;";
#endif

                _conn = new MySqlConnection(sqlConnect);
                _conn.Open();
            }
            catch (Exception e)
            {
                Logger.Error(e);
                MessageBoxUtil.ShowMessageError("Lỗi xảy ra khi kết nối server");
            }
        }
//        public DataTable LoadData(string sql)
//        {
//            if (_conn == null || _conn.State == ConnectionState.Broken || _conn.State == ConnectionState.Closed) OpenConnect();
//            var da = new MySqlDataAdapter(sql, _conn) { SelectCommand = { CommandType = CommandType.Text } };
//            var dt = new DataTable();
//            try
//            {
//                da.Fill(dt);
//            }
//            catch (MySqlException exception)
//            {
//                Logger.Error(exception.Message);
//                Logger.ErrorFormat("Query: {0}", sql);
//                return null;
//            }
//
//            _conn.Close();
//            return dt;
//        }
//        public int Query(string sql)
//        {
//            try
//            {
//                if (_conn == null || _conn.State == ConnectionState.Broken || _conn.State == ConnectionState.Closed) OpenConnect();
//                var cmd = _conn.CreateCommand();
//                cmd.CommandText = sql;
//                var result = cmd.ExecuteNonQuery();
//                _conn.Close();
//                return result;
//            }
//            catch (Exception ex)
//            {
//                Logger.Error(ex.Message);
//                Logger.ErrorFormat("Query: {0}", sql);
//                return QueryFailCode;
//            }
//        }

        internal MySqlConnection GetConnection()
        {
            if (_conn == null || _conn.State == ConnectionState.Broken || _conn.State == ConnectionState.Closed) OpenConnect();
            return _conn;
        }
    }
}