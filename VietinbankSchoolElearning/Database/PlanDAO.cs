﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Documents;
using log4net;
using log4net.Repository.Hierarchy;
using MySql.Data.MySqlClient;
using VietinbankSchoolElearning.DataModel;
using VietinbankSchoolElearning.Models;

namespace VietinbankSchoolElearning.Database
{
    public class PlanDAO
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static void UpdatePlan(PlanData plan)
        {
            UpdatePlans(new List<PlanData> { plan });
        }
        public static void UpdatePlans(List<PlanData> plans)
        {
            using (MySqlConnection connection = DatabaseConnection.Instance.GetConnection())
            {
                using (MySqlTransaction trans = connection.BeginTransaction())
                {
                    try
                    {
                        var cmd = new MySqlCommand
                        {
                            Connection = connection,
                            Transaction = trans,
                            CommandText =
                                "insert into ke_hoach(ma_bai_giang,ten_bai_giang,ma_thay_the,loai_ke_hoach,muc_tieu,doi_tuong,noi_dung,thoi_gian_nhan_slide,thoi_gian_bat_dau,thoi_gian_hoan_thanh,phong_ban_de_xuat,can_bo_nghiep_vu,can_bo_ngan_hang,can_bo_it,lanh_dao_phu_trach) " +
                                "values(@ma_bai_giang,@ten_bai_giang,@ma_thay_the,@loai_ke_hoach,@muc_tieu,@doi_tuong,@noi_dung,@thoi_gian_nhan_slide,@thoi_gian_bat_dau,@thoi_gian_hoan_thanh,@phong_ban_de_xuat,@can_bo_nghiep_vu,@can_bo_ngan_hang,@can_bo_it,@lanh_dao_phu_trach) " +
                                "on duplicate key update ten_bai_giang=@ten_bai_giang,ma_thay_the=@ma_thay_the,loai_ke_hoach=@loai_ke_hoach,muc_tieu=@muc_tieu,doi_tuong=@doi_tuong,noi_dung=@noi_dung," +
                                "thoi_gian_nhan_slide=@thoi_gian_nhan_slide,thoi_gian_bat_dau=@thoi_gian_bat_dau,thoi_gian_hoan_thanh=@thoi_gian_hoan_thanh,phong_ban_de_xuat=@phong_ban_de_xuat," +
                                "can_bo_nghiep_vu=@can_bo_nghiep_vu,can_bo_ngan_hang=@can_bo_ngan_hang,can_bo_it=@can_bo_it,lanh_dao_phu_trach=@lanh_dao_phu_trach"
                        };
                        int count = 0;
                        int i = 0;
                        foreach (var plan in plans)
                        {
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@ma_bai_giang", plan.Id);
                            cmd.Parameters.AddWithValue("@ten_bai_giang", plan.Name);
                            cmd.Parameters.AddWithValue("@ma_thay_the", plan.ReplaceById);
                            cmd.Parameters.AddWithValue("@loai_ke_hoach", plan.PlanType);
                            cmd.Parameters.AddWithValue("@muc_tieu", plan.Goal);
                            cmd.Parameters.AddWithValue("@doi_tuong", plan.StudentType);
                            cmd.Parameters.AddWithValue("@noi_dung", plan.Content);
                            cmd.Parameters.AddWithValue("@thoi_gian_nhan_slide", plan.SlideTime);
                            cmd.Parameters.AddWithValue("@thoi_gian_bat_dau", plan.BeginTime);
                            cmd.Parameters.AddWithValue("@thoi_gian_hoan_thanh", plan.CompleteTime);
                            cmd.Parameters.AddWithValue("@phong_ban_de_xuat", plan.ProposedDeparment);
                            cmd.Parameters.AddWithValue("@can_bo_nghiep_vu", plan.SlideStaff);
                            cmd.Parameters.AddWithValue("@can_bo_ngan_hang", plan.SchoolStaff);
                            cmd.Parameters.AddWithValue("@can_bo_it", plan.ITStaff);
                            cmd.Parameters.AddWithValue("@lanh_dao_phu_trach", plan.LeaderInCharge);
                            cmd.ExecuteNonQuery();
                            count++;
                            i++;
                            if (count == DatabaseConnection.MySqlTransactionLimit || i == plans.Count)
                            {
                                trans.Commit();
                                count = 0;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Logger.Info("Error when update plans", e);
                        trans.Rollback();
                    }
                }
            }
        }

        public static List<PlanData> GetPlanByYear(string year)
        {
            var plans = new List<PlanData>();
            using (MySqlConnection connection = DatabaseConnection.Instance.GetConnection())
            {
                try
                {
                    var cmd = new MySqlCommand
                    {
                        Connection = connection,
                        CommandText = "select * from ke_hoach where ma_bai_giang like @ma_bai_giang"
                    };
                    cmd.Parameters.AddWithValue("@ma_bai_giang", year + "%");
                    MySqlDataReader reader = cmd.ExecuteReader();
                    var dt = new DataTable();
                    dt.Load(reader);
                    plans.AddRange(from DataRow row in dt.Rows
                                   select new PlanData
                                   {
                                       Id = row["ma_bai_giang"].ToString(),
                                       Name = row["ten_bai_giang"].ToString(),
                                       ReplaceById = row["ma_thay_the"].ToString(),
                                       PlanType = row["loai_ke_hoach"].ToString(),
                                       Goal = row["muc_tieu"].ToString(),
                                       StudentType = row["doi_tuong"].ToString(),
                                       Content = row["noi_dung"].ToString(),
                                       SlideTime = row["thoi_gian_nhan_slide"].ToString(),
                                       BeginTime = row["thoi_gian_bat_dau"].ToString(),
                                       CompleteTime = row["thoi_gian_hoan_thanh"].ToString(),
                                       ProposedDeparment = row["phong_ban_de_xuat"].ToString(),
                                       SchoolStaff = row["can_bo_ngan_hang"].ToString(),
                                       ITStaff = row["can_bo_it"].ToString(),
                                       LeaderInCharge = row["lanh_dao_phu_trach"].ToString(),
                                       SlideStaff = row["can_bo_nghiep_vu"].ToString()
                                   });
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    plans = new List<PlanData>();
                }
            }
            return plans;
        }

        public static int GetMaxIndex(int year)
        {
            using (MySqlConnection connection = DatabaseConnection.Instance.GetConnection())
            {
                try
                {
                    var cmd = new MySqlCommand
                    {
                        Connection = connection,
                        CommandText = "select ma_bai_giang from ke_hoach where ma_bai_giang like @ma_bai_giang order by ma_bai_giang desc limit 1"
                    };
                    cmd.Parameters.AddWithValue("@ma_bai_giang", year + "%");
                    return int.Parse(cmd.ExecuteScalar().ToString());

                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return -1;
                }
            }
        }

        public static bool CancelPlan(string id)
        {
            using (MySqlConnection connection = DatabaseConnection.Instance.GetConnection())
            {
                try
                {
                    var cmd = new MySqlCommand
                    {
                        Connection = connection,
                        CommandText = "update ke_hoach set huy=TRUE where ma_bai_giang=@ma_bai_giang"
                    };
                    cmd.Parameters.AddWithValue("@ma_bai_giang", id);
                    object executeScalar = cmd.ExecuteScalar();
                    return executeScalar != null;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return false;
                }
            }
        }
    }
}