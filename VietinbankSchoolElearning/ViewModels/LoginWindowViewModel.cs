﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using Caliburn.Micro;
using VietinbankSchoolElearning.Models;


namespace VietinbankSchoolElearning.ViewModels
{
    [Export(typeof(LoginWindowViewModel))]
    public class LoginWindowViewModel : Screen
    {
        private readonly IWindowManager _windowManager;
        private readonly IEventAggregator _eventAggregator;
        private readonly LoginWindowModel _model = new LoginWindowModel();

        [ImportingConstructor]
        public LoginWindowViewModel(IWindowManager windowManager, IEventAggregator eventAggregator)
        {
            _windowManager = windowManager;
            _eventAggregator = eventAggregator;
#if DEBUG
            Username = "user";
            Password = "1234";
#endif
        }
        #region binding
        private string _username;

        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                NotifyOfPropertyChange(() => Username);
            }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                NotifyOfPropertyChange(() => Password);
            }
        }

        private bool _errorVisibility = false;

        public bool ErrorVisibility
        {
            get { return _errorVisibility; }
            set
            {
                _errorVisibility = value;
                NotifyOfPropertyChange(() => ErrorVisibility);
            }
        }

        #endregion

        public void Login(object parameter)
        {
            ErrorVisibility = false;

            if ( _model.Login(Username, Password))
            {
                _windowManager.ShowWindow(new MainWindowViewModel(_windowManager, _eventAggregator));
                TryClose();
            }
            else
            {
                ErrorVisibility = true;
            }
        }
    }
}
