﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Caliburn.Micro;
using VietinbankSchoolElearning.Database;
using VietinbankSchoolElearning.DataModel;
using VietinbankSchoolElearning.Message;
using VietinbankSchoolElearning.Models;

namespace VietinbankSchoolElearning.ViewModels
{
    [Export(typeof(PlanEditorWindowViewModel))]
    public class PlanEditorWindowViewModel : Screen, IHandle<KeyValuePair<string, PlanData>>
    {
        private readonly IEventAggregator _eventAggregator;
        private PlanData _plan;
        private PlanWindowEditorModel _model = new PlanWindowEditorModel();
        [ImportingConstructor]
        public PlanEditorWindowViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }

        #region binding properties

        private string _title = string.Empty;
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                NotifyOfPropertyChange(() => Title);
            }
        }

        private string _id = string.Empty;
        public string Id
        {
            get { return _id; }
            set
            {
                _id = value;
                if (_plan != null) _plan.Id = _id;
                NotifyOfPropertyChange(() => Id);
            }
        }

        private string _name = string.Empty;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                if (_plan != null) _plan.Name = _name;
                NotifyOfPropertyChange(() => Name);
            }
        }

        private string _replaceById = string.Empty;
        public string ReplaceById
        {
            get { return _replaceById; }
            set
            {
                _replaceById = value;
                if (_plan != null) _plan.ReplaceById = _replaceById;
                NotifyOfPropertyChange(() => ReplaceById);
            }
        }

        private string _planType = string.Empty;
        public string PlanType
        {
            get { return _planType; }
            set
            {
                _planType = value;
                if (_plan != null) _plan.PlanType = _planType;
                NotifyOfPropertyChange(() => PlanType);
            }
        }

        private string _goal = string.Empty;
        public string Goal
        {
            get { return _goal; }
            set
            {
                _goal = value;
                if (_plan != null) _plan.Goal = _goal;
                NotifyOfPropertyChange(() => Goal);
            }
        }
        private string _studentType = string.Empty;
        public string StudentType
        {
            get { return _studentType; }
            set
            {
                _studentType = value;
                if (_plan != null) _plan.StudentType = _studentType;
                NotifyOfPropertyChange(() => StudentType);
            }
        }

        private string _content = string.Empty;
        public string Content
        {
            get { return _content; }
            set
            {
                _content = value;
                if (_plan != null) _plan.Content = _content;
                NotifyOfPropertyChange(() => Content);
            }
        }

        private string _slideTime = string.Empty;
        public string SlideTime
        {
            get { return _slideTime; }
            set
            {
                _slideTime = value;
                if (_plan != null) _plan.SlideTime = _slideTime;
                NotifyOfPropertyChange(() => SlideTime);
            }
        }

        private string _beginTime = string.Empty;
        public string BeginTime
        {
            get { return _beginTime; }
            set
            {
                _beginTime = value;
                if (_plan!=null) _plan.SlideTime = _beginTime;
                NotifyOfPropertyChange(() => BeginTime);
            }
        }

        private string _completeTime = string.Empty;
        public string CompleteTime
        {
            get { return _completeTime; }
            set
            {
                _completeTime = value;
                if (_plan != null) _plan.CompleteTime = _completeTime;
                NotifyOfPropertyChange(() => CompleteTime);
            }
        }

        private string _proposedDeparment = string.Empty;
        public string ProposedDeparment
        {
            get { return _proposedDeparment; }
            set
            {
                _proposedDeparment = value;
                if (_plan != null) _plan.ProposedDeparment = _proposedDeparment;
                NotifyOfPropertyChange(() => ProposedDeparment);
            }
        }

        private string _slideStaff = string.Empty;
        public string SlideStaff
        {
            get { return _slideStaff; }
            set
            {
                _slideStaff = value;
                if (_plan != null) _plan.SlideStaff = _slideStaff;
                NotifyOfPropertyChange(() => SlideStaff);
            }
        }

        private string _schoolStaff = string.Empty;
        public string SchoolStaff
        {
            get { return _schoolStaff; }
            set
            {
                _schoolStaff = value;
                if (_plan != null) _plan.SchoolStaff = _schoolStaff;
                NotifyOfPropertyChange(() => SchoolStaff);
            }
        }

        private string _itStaff = string.Empty;
        public string ITStaff
        {
            get { return _itStaff; }
            set
            {
                _itStaff = value;
                if (_plan != null) _plan.ProposedDeparment = _itStaff;
                NotifyOfPropertyChange(() => ITStaff);
            }
        }
        private string _leaderInCharge = string.Empty;
        public string LeaderInCharge
        {
            get { return _leaderInCharge; }
            set
            {
                _leaderInCharge = value;
                if (_plan != null) _plan.LeaderInCharge = _leaderInCharge;
                NotifyOfPropertyChange(() => LeaderInCharge);
            }
        }
        #endregion

        public void Handle(KeyValuePair<string, PlanData> message)
        {
            _plan = message.Value;
            string year = message.Key;
            if (string.IsNullOrWhiteSpace(_plan.Id))
            {
                Title = "Thêm bài giảng";
                Id = _model.GetNextId(year);
            }
            else
            {
                Title = "Sửa thông tin bài giảng";
                Id = _plan.Id;
            }
            Name = _plan.Name;
            ReplaceById = _plan.ReplaceById;
            PlanType = _plan.PlanType;
            Goal = _plan.Goal;
            StudentType = _plan.StudentType;
            Content = _plan.Content;
            SlideTime = _plan.SlideTime;
            BeginTime = _plan.BeginTime;
            CompleteTime = _plan.BeginTime;
            ProposedDeparment = _plan.ProposedDeparment;
            SlideStaff = _plan.SlideStaff;
            SchoolStaff = _plan.SchoolStaff;
            ITStaff = _plan.ITStaff;
            LeaderInCharge = _plan.LeaderInCharge;
        }


        public void UpdatePlan()
        {
            _model.UpdatePlan(_plan);
            _eventAggregator.PublishOnUIThread(new ClosePlanEditorMessage());
            TryClose();
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }
    }
}