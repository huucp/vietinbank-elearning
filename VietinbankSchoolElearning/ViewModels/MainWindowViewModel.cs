﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using Caliburn.Micro;
using Caliburn.Micro.DevExpress;
using VietinbankSchoolElearning.Message;

namespace VietinbankSchoolElearning.ViewModels
{
    [Export(typeof(MainWindowViewModel))]
    public class MainWindowViewModel : DevExpressConductorOneActive<IScreen>,IHandle<ShowWaitIndicatorMessage>
    {
        private readonly IWindowManager _windowManager;
        private readonly IEventAggregator _eventAggregator;

        [ImportingConstructor]
        public MainWindowViewModel(IWindowManager windowManager, IEventAggregator eventAggregator)
        {
            _windowManager = windowManager;
            _eventAggregator = eventAggregator;
#if DEBUG
            ActiveStaffView();
#else
            ActivePlanView();
#endif
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            _eventAggregator.Subscribe(this);
        }

        protected override void OnDeactivate(bool close)
        {
            base.OnDeactivate(close);
            _eventAggregator.Unsubscribe(this);
        }

        #region binding

        private bool _showWaitIndicator = true;

        public bool ShowWaitIndicator
        {
            get { return _showWaitIndicator; }
            set
            {
                if (value.Equals(_showWaitIndicator)) return;
                _showWaitIndicator = value;
                NotifyOfPropertyChange(() => ShowWaitIndicator);
            }
        }

        #endregion

        private bool CheckActiveView(Type viewType)
        {
            return ActiveItemIndex != -1 && Items[ActiveItemIndex].GetType() == viewType;
        }

        public void ActivePlanView()
        {
            if (CheckActiveView(typeof(PlanControlViewModel))) return;
            ActivateItem(new PlanControlViewModel(_windowManager, _eventAggregator) { DisplayName = "Kế hoạch năm", CloseAction = CloseAction });
        }

        public void ActiveStaffView()
        {
            if (CheckActiveView(typeof(StaffControlViewModel))) return;
            ActivateItem(new StaffControlViewModel(_windowManager, _eventAggregator) { DisplayName = "Quản lý cán bộ", CloseAction = CloseAction });
        }

        public void ActivePartnerView()
        {

        }


        public void Handle(ShowWaitIndicatorMessage message)
        {
            ShowWaitIndicator = message.Show;
        }
    }
}