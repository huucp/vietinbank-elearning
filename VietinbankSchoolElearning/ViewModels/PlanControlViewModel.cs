﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Dynamic;
using System.Globalization;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using Caliburn.Micro;
using Caliburn.Micro.DevExpress;
using DevExpress.Xpf.Editors;
using log4net;
using VietinbankSchoolElearning.DataModel;
using VietinbankSchoolElearning.Message;
using VietinbankSchoolElearning.Models;
using VietinbankSchoolElearning.Properties;
using VietinbankSchoolElearning.Utils;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace VietinbankSchoolElearning.ViewModels
{
    [Export(typeof(PlanControlViewModel))]
    public class PlanControlViewModel : DXDockingScreen, IHandle<ClosePlanEditorMessage>
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly PlanControlModel _model = new PlanControlModel();
        private string _importFilename;
        private readonly IWindowManager _windowManager;
        private IEventAggregator _eventAggregator;
        public PlanControlViewModel(IWindowManager windowManager, IEventAggregator eventAggregator)
        {
            _windowManager = windowManager;
            _eventAggregator = eventAggregator;
            IsPopupOpen = false;
            LoadData(Resources.PlanBeginYear);
            Year = new BindableCollection<string>(_model.GetListYear());
        }

        protected override void OnActivate()
        {
            _eventAggregator.Subscribe(this);
            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _eventAggregator.Unsubscribe(this);
            base.OnDeactivate(close);
        }



        #region binding property
        private IObservableCollection<PlanData> _planList = new BindableCollection<PlanData>();
        public IObservableCollection<PlanData> PlanList
        {
            get { return _planList; }
            set
            {
                _planList = value;
                NotifyOfPropertyChange(() => PlanList);
            }

        }

        private IObservableCollection<string> _year = new BindableCollection<string>();
        public IObservableCollection<string> Year
        {
            get { return _year; }
            set
            {
                _year.Clear();
                _year.AddRange(value);
                NotifyOfPropertyChange(() => Year);
            }
        }

        private bool _isPopupOpen = false;
        public bool IsPopupOpen
        {
            get { return _isPopupOpen; }
            set
            {
                _isPopupOpen = value;
                NotifyOfPropertyChange(() => IsPopupOpen);
            }
        }

        private int _importYearIndex = 0;
        public int ImportYearIndex
        {
            get { return _importYearIndex; }
            set
            {
                _importYearIndex = value;
                NotifyOfPropertyChange(() => ImportYearIndex);
            }
        }

        private int _yearIndex = 0;
        public int YearIndex
        {
            get { return _yearIndex; }
            set
            {
                _yearIndex = value;
                NotifyOfPropertyChange(() => YearIndex);
            }
        }

        private int _planIndex = 0;
        public int PlanIndex
        {
            get { return _planIndex; }
            set
            {
                _planIndex = value;
                NotifyOfPropertyChange(() => PlanIndex);
            }
        }
        #endregion
        public void ImportExcel()
        {
            _importFilename = _model.OpenFileDialog();
            IsPopupOpen = true;
            //LoadData(Resources.PlanBeginYear);
        }

        private void LoadData(int year)
        {
            PlanList.Clear();
            List<PlanData> plansByYear = _model.GetPlansByYear(year.ToString(CultureInfo.InvariantCulture));
            PlanList.AddRange(plansByYear);
        }

        public void YearComboBoxChanged(ComboBoxEdit cbe)
        {
            int year = int.Parse(cbe.EditValue.ToString());
            LoadData(year);
        }

        public void SearchTextChanged(TextBox tb)
        {
            List<PlanData> searchPlans = _model.SearchPlans(tb.Text);
            PlanList.Clear();
            PlanList.AddRange(searchPlans);
        }

        public void ImportYearButtonClick()
        {
            IsPopupOpen = false;
            int year = int.Parse(Year[ImportYearIndex]);
            if (string.IsNullOrWhiteSpace(_importFilename))
            {
                MessageBoxUtil.ShowMessageWarn("Chưa chọn file import!");
                return;
            }
            _model.ImportFromExcel(_importFilename, year);
            LoadData(year);
            YearIndex = ImportYearIndex;
        }

        public void CancelImport()
        {
            IsPopupOpen = false;
        }

        public void EditPlan()
        {
            var plan = _planList[PlanIndex];
            string year = Year[YearIndex];
            _windowManager.ShowWindow(new PlanEditorWindowViewModel(_eventAggregator));
            _eventAggregator.PublishOnUIThread(new KeyValuePair<string, PlanData>(year, plan));
        }

        public void AddPlan()
        {
            string year = Year[YearIndex];
            _windowManager.ShowWindow(new PlanEditorWindowViewModel(_eventAggregator));
            _eventAggregator.PublishOnUIThread(new KeyValuePair<string, PlanData>(year, new PlanData()));
        }

        public void CancelPlan()
        {
            if (IsValidPlanIndex())
            {
                PlanControlModel.CancelPlan(_planList[PlanIndex].Id);
            }
        }

        private bool IsValidPlanIndex()
        {
            if (PlanIndex == -1)
            {
                MessageBoxUtil.ShowMessageInfo("Phải chọn lớp trước!");
                return false;
            }
            return true;
        }

        public void Handle(ClosePlanEditorMessage message)
        {
            LoadData(int.Parse(Year[YearIndex]));
        }
    }
}