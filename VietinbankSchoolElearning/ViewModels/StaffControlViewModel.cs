﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Caliburn.Micro.DevExpress;
using VietinbankSchoolElearning.DataModel;
using VietinbankSchoolElearning.Message;
using VietinbankSchoolElearning.Models;

namespace VietinbankSchoolElearning.ViewModels
{
    public class StaffControlViewModel : DXDockingScreen
    {
        private IWindowManager _windowManager;
        private IEventAggregator _eventAggregator;
        private StaffControlModel _model = new StaffControlModel();
        private List<StaffData> _allStafs = new List<StaffData>();
        public StaffControlViewModel(IWindowManager windowManager, IEventAggregator eventAggregator)
        {
            _windowManager = windowManager;
            _eventAggregator = eventAggregator;
            LoadData();
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            _eventAggregator.Subscribe(this);
        }

        protected override void OnDeactivate(bool close)
        {
            base.OnDeactivate(close);
            _eventAggregator.Unsubscribe(this);
        }

        #region binding

        private string _searchKeyword = string.Empty;

        public string SearchKeyword
        {
            get { return _searchKeyword; }
            set
            {
                if (value == _searchKeyword) return;
                _searchKeyword = value;
                NotifyOfPropertyChange(() => SearchKeyword);
            }
        }

        private IObservableCollection<StaffData> _staffs = new BindableCollection<StaffData>();

        public IObservableCollection<StaffData> Staffs
        {
            get { return _staffs; }
            set
            {
                if (Equals(value, _staffs)) return;
                _staffs = value;
                NotifyOfPropertyChange(() => Staffs);
            }
        }

        private string _userEmail = string.Empty;

        public string UserEmail
        {
            get { return _userEmail; }
            set
            {
                if (value == _userEmail) return;
                _userEmail = value;
                NotifyOfPropertyChange(() => UserEmail);
            }
        }

        private bool _isPopupOpen = false;
        public bool IsPopupOpen
        {
            get { return _isPopupOpen; }
            set
            {
                _isPopupOpen = value;
                NotifyOfPropertyChange(() => IsPopupOpen);
            }
        }

        private int _userTypeIndex = 0;

        public int UserTypeIndex
        {
            get { return _userTypeIndex; }
            set
            {
                if (value == _userTypeIndex) return;
                _userTypeIndex = value;
                NotifyOfPropertyChange(() => UserTypeIndex);
            }
        }

        private int _staffIndex = 0;

        public int StaffIndex
        {
            get { return _staffIndex; }
            set
            {
                if (value == _staffIndex) return;
                _staffIndex = value;
                NotifyOfPropertyChange(() => StaffIndex);
            }
        }

        #endregion

        public void SearchTextChanged()
        {
            Console.WriteLine("key:" + SearchKeyword);
        }

        public void AddUser()
        {
            IsPopupOpen = true;
        }

        public void AddUserWithEmail()
        {
            IsPopupOpen = false;
            _eventAggregator.PublishOnUIThread(new ShowWaitIndicatorMessage(true));
            if (_model.AddUser(UserEmail, (UserType) UserTypeIndex))
            {
                // TODO: show susccess message here
            }
            else {
                // TODO: show fail message here
            }
            LoadData();
            _eventAggregator.PublishOnUIThread(new ShowWaitIndicatorMessage(false));
        }

        public void CancelAddUser()
        {
            IsPopupOpen = false;
        }

        public void DeleteUser()
        {

        }


        private void LoadData()
        {
            _allStafs.Clear();
            _allStafs.AddRange(_model.GetStaffs());
            Staffs.Clear();
            Staffs.AddRange(_allStafs);
        }
    }
}