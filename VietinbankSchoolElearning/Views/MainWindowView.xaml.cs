﻿using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Bars;
using DevExpress.Xpf.Ribbon;
using VietinbankSchoolElearning.ViewModels;

namespace VietinbankSchoolElearning.Views
{
    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : DXRibbonWindow 
    {
        public MainWindowView()
        {
            InitializeComponent();
        }

        // Cannot bind action to BarItem because it inherits from FrameworkContentElement.
        // So use trick.
        private void PlanButton_OnItemClick(object sender, ItemClickEventArgs e)
        {

            ((MainWindowViewModel)DataContext).ActivePlanView();
        }

        private void PartnerButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            ((MainWindowViewModel)DataContext).ActivePartnerView();
        }

        private void InternalLecturerButton_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void HiredLecturerButton_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void StaffButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            ((MainWindowViewModel)DataContext).ActiveStaffView();
        }
    }
}
