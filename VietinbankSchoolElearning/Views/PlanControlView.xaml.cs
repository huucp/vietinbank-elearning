﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VietinbankSchoolElearning.Views
{
    /// <summary>
    /// Interaction logic for PlanControlView.xaml
    /// </summary>
    public partial class PlanControlView : UserControl
    {
        public PlanControlView()
        {
            InitializeComponent();
            ColumnVisibilityButton_Click(null, null);
        }

        private bool _isColumnHidden = false;
        private void ColumnVisibilityButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (_isColumnHidden)
            {
                ColumnVisibilty.Content = "Ẩn cột";
                PlanList.Columns[4].Visibility = Visibility.Visible;
                PlanList.Columns[5].Visibility = Visibility.Visible;
                PlanList.Columns[6].Visibility = Visibility.Visible;
                PlanList.Columns[8].Visibility = Visibility.Visible;
                _isColumnHidden = false;
            }
            else
            {
                ColumnVisibilty.Content = "Hiện toàn bộ";
                PlanList.Columns[4].Visibility = Visibility.Collapsed;
                PlanList.Columns[5].Visibility = Visibility.Collapsed;
                PlanList.Columns[6].Visibility = Visibility.Collapsed;
                PlanList.Columns[8].Visibility = Visibility.Collapsed;
                _isColumnHidden = true;
            }
        }
    }
}
