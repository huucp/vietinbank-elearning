﻿namespace VietinbankSchoolElearning.Message
{
    public class ShowWaitIndicatorMessage
    {
        public bool Show { get; set; }

        public ShowWaitIndicatorMessage(bool show)
        {
            Show = show;
        }
    }
}