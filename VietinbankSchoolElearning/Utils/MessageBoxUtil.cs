﻿using System.Windows;
using DevExpress.Xpf.Core;

namespace VietinbankSchoolElearning.Utils
{
    public static class MessageBoxUtil
    {
        public static void ShowMessageError(string msg)
        {
           DXMessageBox.Show(msg, "Lỗi", MessageBoxButton.OK,MessageBoxImage.Error);
        }

        public static void ShowMessageInfo(string msg)
        {
            DXMessageBox.Show(msg,"Thông tin",MessageBoxButton.OK, MessageBoxImage.Information);
        }


        public static void ShowMessageWarn(string msg)
        {
            DXMessageBox.Show(msg, "Cảnh báo", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public static bool ShowMessageConfirm(string msg)
        {
            return DXMessageBox.Show(msg, "Xác nhận thông tin", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes;
        }
    }
}