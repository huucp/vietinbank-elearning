﻿using System;
using System.Globalization;

namespace VietinbankSchoolElearning.Utils
{
    public static class UtilityFunc
    {
        public static string GetCellValueInString(Microsoft.Office.Interop.Excel.Range cell)
        {
            if (cell.Value2 == null) return string.Empty;
            return cell.Value2.ToString();
        }

        public static int GetCurrentYear()
        {
            return DateTime.Now.Year;
        }

        public static DateTime StringToTime(string s, string format = ConstParams.DefaultDateFormat)
        {
            return DateTime.ParseExact(s, format, CultureInfo.InvariantCulture);
        }

        public static int CompareStringDate(string d1, string d2, string format = ConstParams.DefaultDateFormat)
        {
            var date1 = StringToTime(d1, format);
            var date2 = StringToTime(d2, format);

            if (date1 > date2) return 1;
            if (date1 == date2) return 0;
            return -1;
        }

        public static string GetCurrentTime(string format = ConstParams.DefaultDateFormat)
        {
            return DateTime.Now.ToString(format, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// When date have Q1,Q2,Q3,Q4
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string NormalizeDate(string date)
        {
            string newDate = date;
            if (date.Contains("Q1"))
            {
                newDate = date.Replace("Q1","31/03");

            }
            else if (date.Contains("Q2"))
            {
                newDate = date.Replace("Q2", "30/06");

            }
            else if (date.Contains("Q3"))
            {
                newDate = date.Replace("Q3", "30/09");

            }
            else if (date.Contains("Q4"))
            {
                newDate = date.Replace("Q1", "31/12");

            }
            return newDate;
        }

        public static string GetCellTimeString(Microsoft.Office.Interop.Excel.Range cell)
        {
            string cellValueInString = GetCellValueInString(cell);
            return NormalizeDate(cellValueInString);
        }
    }
}