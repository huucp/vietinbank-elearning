﻿using System.Configuration;

namespace VietinbankSchoolElearning.Utils
{
    public class Config
    {
        public static string ServerIp
        {
            get { return ConfigurationManager.AppSettings["serverIP"]; }
        }
    }
}