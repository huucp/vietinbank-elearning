﻿namespace VietinbankSchoolElearning.Utils
{
    public class ConstParams
    {
        public const string DefaultExcelFilters = @"Excel files (*.xls,*.xlsx)|*.xls;*.xlsx|All files (*.*)|*.*";
        public const string DefaultDateFormat = "dd/MM/yyyy";
        public const string DefaultPassword = "vtbelearning";        


    }
}