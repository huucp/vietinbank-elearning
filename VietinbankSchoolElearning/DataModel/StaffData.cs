﻿namespace VietinbankSchoolElearning.DataModel
{
    public class StaffData
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Title { get;set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string AccountNumber { get; set; }
        public UserType StaffType { get; set; }
    }
}