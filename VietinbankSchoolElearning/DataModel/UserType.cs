﻿namespace VietinbankSchoolElearning.DataModel
{
    public enum UserType
    {
        Staff,
        Leader,
        Admin
    }
}