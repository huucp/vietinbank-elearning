﻿using System;
using System.Collections.Generic;

namespace VietinbankSchoolElearning.DataModel
{
    public class PlanData
    {
        private const char StaffDelimiter = ';';
        public enum PlanStatus
        {
            NotInTime,
            InProgress,
            SlideOutdate,
            AcceptanceOutdate,
            Cancel,
            Pause,
            InPaying,
            Complete
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string ReplaceById { get; set; }
        public string PlanType { get; set; }
        public string Goal { get; set; }
        public string StudentType { get; set; }
        public string Content { get; set; }
        public string SlideTime { get; set; }
        public string BeginTime { get; set; }
        public string CompleteTime { get; set; }
        public string ProposedDeparment { get; set; }
        public List<string> SlideStaffList = new List<string>();

        public String SlideStaff
        {
            get
            {
                if (SlideStaffList.Count == 0) return string.Empty;
                string s = SlideStaffList[0];
                for (var i = 1; i < SlideStaffList.Count; i++)
                {
                    s += StaffDelimiter + SlideStaffList[i];
                }
                return s;
            }
            set
            {
                string[] split = value.Split(StaffDelimiter);
                SlideStaffList.Clear();
                SlideStaffList.AddRange(split);
            }
        }
        public string SchoolStaff { get; set; }
        public string ITStaff { get; set; }
        public string LeaderInCharge { get; set; }

        public PlanStatus Status
        {
            get
            {
//                if (UtilityFunc.CompareStringDate(SlideTime, UtilityFunc.GetCurrentTime()) < 0)
//                {
//                    return PlanStatus.NotInTime;
//                }

                return PlanStatus.Complete;
            }
        }

        public PlanData()
        {
            Id = string.Empty;
            Name = string.Empty;
            ReplaceById = string.Empty;
            PlanType = string.Empty;
            Goal = string.Empty;
            StudentType = string.Empty;
            Content = string.Empty;
            SlideTime = string.Empty;
            BeginTime = string.Empty;
            CompleteTime = string.Empty;
            ProposedDeparment = string.Empty;
            SchoolStaff = string.Empty;
            ITStaff = string.Empty;
            LeaderInCharge = string.Empty;
        }
    }
}
