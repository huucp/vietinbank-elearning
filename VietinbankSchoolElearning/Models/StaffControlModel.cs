﻿using System.Collections.Generic;
using System.Windows.Documents;
using VietinbankSchoolElearning.Database;
using VietinbankSchoolElearning.DataModel;
using VietinbankSchoolElearning.Utils;

namespace VietinbankSchoolElearning.Models
{
    public class StaffControlModel
    {
        public bool AddUser(string userEmail, UserType type)
        {
            return StaffDAO.AddUser(userEmail,ConstParams.DefaultPassword,type);
        }

        public List<StaffData> GetStaffs()
        {
            return StaffDAO.GetStaffs();
        }

        public bool DeleteUser(string email)
        {
            return StaffDAO.DeleteUser(email);
        }

    }
}