﻿using VietinbankSchoolElearning.Database;

namespace VietinbankSchoolElearning.Models
{
    public class LoginWindowModel
    {
        public bool Login(string username, string password)
        {
            return LoginDAO.Login(username, password);
        }
    }
}