﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using log4net;
using Microsoft.Office.Interop.Excel;
using Microsoft.Win32;
using VietinbankSchoolElearning.Database;
using VietinbankSchoolElearning.DataModel;
using VietinbankSchoolElearning.Properties;
using VietinbankSchoolElearning.Utils;

namespace VietinbankSchoolElearning.Models
{
    public class PlanControlModel
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private List<PlanData> _currentPlans = new List<PlanData>();

        public string OpenFileDialog()
        {
            var openFileDialog = new OpenFileDialog { Filter = ConstParams.DefaultExcelFilters };

            if (openFileDialog.ShowDialog() == true)
            {
                return openFileDialog.FileName;
            }
            return string.Empty;
        }
        public void ImportFromExcel(string filename, int year)
        {
            var plans = new List<PlanData>();

            var app = new Application { Visible = false };
            var workBook = app.Workbooks.Open(filename);
            Worksheet sheet = workBook.ActiveSheet;
            int begin = PlanDAO.GetMaxIndex(year);
            if (begin == -1)
            {
                begin = year * 1000 + 1;
            }
            else
            {
                begin++;
            }
            try
            {

                var maxRow = sheet.UsedRange.Rows.Count;
                int startExcelIndex = 5;
                for (var i = startExcelIndex; i <= maxRow; i++)
                {
                    string index = UtilityFunc.GetCellValueInString(sheet.Cells[i, "A"]);
                    string slideStarff = UtilityFunc.GetCellValueInString(sheet.Cells[i, "Q"]);
                    if (string.IsNullOrWhiteSpace(index))
                    {
                        if (string.IsNullOrWhiteSpace(slideStarff)) break; // end of sheet
                        else
                        {
                            var last = plans.Last();
                            last.SlideStaffList.Add(slideStarff);
                            continue;
                        }
                    }
                    string id = (begin + i - startExcelIndex).ToString();
                    string name = UtilityFunc.GetCellValueInString(sheet.Cells[i, "B"]);
                    string replaceId = UtilityFunc.GetCellValueInString(sheet.Cells[i, "C"]);
                    string planType = UtilityFunc.GetCellValueInString(sheet.Cells[i, "D"]);
                    string goal = UtilityFunc.GetCellValueInString(sheet.Cells[i, "E"]);
                    string studentType = UtilityFunc.GetCellValueInString(sheet.Cells[i, "F"]);
                    string content = UtilityFunc.GetCellValueInString(sheet.Cells[i, "G"]);
                    string slideTime = UtilityFunc.GetCellTimeString(sheet.Cells[i, "L"]);
                    string beginTime = UtilityFunc.GetCellTimeString(sheet.Cells[i, "M"]);
                    string completeTime = UtilityFunc.GetCellTimeString(sheet.Cells[i, "N"]);
                    string proposedDeparment = UtilityFunc.GetCellValueInString(sheet.Cells[i, "S"]);
                    string schoolStaff = UtilityFunc.GetCellValueInString(sheet.Cells[i, "T"]);
                    string itStaff = UtilityFunc.GetCellValueInString(sheet.Cells[i, "W"]);
                    string leader = UtilityFunc.GetCellValueInString(sheet.Cells[i, "U"]);

                    plans.Add(new PlanData
                    {
                        Id = id,
                        Name = name,
                        ReplaceById = replaceId,
                        PlanType = planType,
                        Goal = goal,
                        StudentType = studentType,
                        Content = content,
                        SlideTime = slideTime,
                        BeginTime = beginTime,
                        CompleteTime = completeTime,
                        ProposedDeparment = proposedDeparment,
                        SlideStaffList = new List<string> { slideStarff },
                        SchoolStaff = schoolStaff,
                        ITStaff = itStaff,
                        LeaderInCharge = leader
                    });
                }
                PlanDAO.UpdatePlans(plans);
            }
            catch (Exception exception)
            {
                Logger.Error("Import course plan failed", exception);
            }
            finally
            {
                workBook.Close(false);
                app.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                //                    FormUtil.CloseWaitForm();
                MessageBoxUtil.ShowMessageInfo(string.Format("Đã import thành công {0} bài giảng", plans.Count));

            }

        }

        public List<PlanData> GetPlansByYear(string year)
        {
            List<PlanData> plansByYear = PlanDAO.GetPlanByYear(year);
            _currentPlans.Clear();
            _currentPlans.AddRange(plansByYear);
            return plansByYear;
        }

        public List<String> GetListYear()
        {
            var list = new List<string>();
            for (int i = Resources.PlanBeginYear; i <= UtilityFunc.GetCurrentYear() + 1; i++)
            {
                list.Add(i.ToString(CultureInfo.InvariantCulture));
            }
            return list;
        }

        public List<PlanData> SearchPlans(string keyword)
        {
            return _currentPlans.Where(plan => plan.Name.Contains(keyword) || plan.ProposedDeparment.Contains(keyword)).ToList();
        }

        public static bool CancelPlan(string id)
        {
            return PlanDAO.CancelPlan(id);
        }
    }
}