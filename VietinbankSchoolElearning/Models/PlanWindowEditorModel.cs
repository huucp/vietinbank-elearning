﻿using System.Globalization;
using System.Reflection;
using log4net;
using VietinbankSchoolElearning.Database;
using VietinbankSchoolElearning.DataModel;

namespace VietinbankSchoolElearning.Models
{
    public class PlanWindowEditorModel
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void UpdatePlan(PlanData plan)
        {
            PlanDAO.UpdatePlan(plan);
        }

        public string GetNextId(string year)
        {
            int y = int.Parse(year);
            int maxIndex = PlanDAO.GetMaxIndex(y);
            if (maxIndex < y) return (y*1000 + 1).ToString(CultureInfo.InvariantCulture);
            return (maxIndex + 1).ToString(CultureInfo.InvariantCulture);
        }
    }
}